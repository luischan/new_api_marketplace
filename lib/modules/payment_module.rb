module PaymentModule

  def get_payment(filter)

    payments = Payment.includes(:client, :subscriptions, :payment_suscriptions).joins(:subscriptions, :client, :payment_suscriptions).all

    if filter[:client_ids].present?
      payments = payments.where("payments.client_id IN (?)", filter[:client_ids])
    end

    if filter[:client_id].present?
      payments = payments.where(:client_id => filter[:client_id])
    end

    if filter[:total].present?
      payments = payments.where(:total => filter[:suscription_type])
    end

    if filter[:date_payment].present?
      payments = payments.where("DATE(payments.date_payment) = DATE(?)" , filter[:date_payment])
    end

    if filter[:date_payment].present? && filter[:date_payment_last].present?
      payments = payments.where("DATE(payments.date_payment) BETWEEN DATE(?) AND DATE(?)" , filter[:date_payment],filter[:date_payment_last] )
    end

    if filter[:suscription_type]
      payments = payments.where("subscriptions.suscription_type = ?" , filter[:suscription_type])
    end

    payments.to_json( :include => [:client, :subscriptions => {:include => [:services] }], :except => [:created_at, :updated_at] )
  end


end