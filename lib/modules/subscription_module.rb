module SubscriptionModule

  def get_subscription(filter)
    subscriptions = Subscription.where(:active => true)

    if filter[:client_id].present?
      subscriptions.where(:client_id => filter[:client_id])
    end

    if filter[:subscription_ids].present?
      subscriptions.where(:id => filter[:subscription_id])
    end

    if filter[:next_payment_date].present?
      date = Time.parse(filter[:next_payment_date]).to_s
      subscriptions.where("DATE(next_payment_date) = DATE('#{date}')")
    end

    subscriptions
  end

  def calculate_next_date_payment(last_date, subscription_type)
    response = {:message => "Generado correctamente",:ok => true, :status => :created, date:nil}
    # Se realiza un en un case debido a que pudiera existir a futuro un nuevo tipo de subscription
    last_date_parse = Time.parse(last_date.to_s)
    case subscription_type.to_i
    when Subscription.periodicities[:month]
      response[:date] = last_date_parse + 1.month
    when Subscription.periodicities[:year]
      response[:date] = last_date_parse + 1.year
    else
      response[:message] = "No se encuentra el tipo de suscripcion"
      response[:status] = :not_found
      response[:ok] = false
    end
    response
  end

  def payment_subscription(ids)
    subscription =  Subscription.includes(:services).where(:id => ids)
    payment_subscriptions = []
    totals = {total:0, subtotal: 0, quantity: 0 }
    date_now =  subscription.first[:next_payment_date]
    payment = {}
    if !subscription.empty?
      if validate_subscription_date(ids, date_now)
        subscription.each do |s|
          new_date = calculate_next_date_payment(date_now, s.suscription_type)
          if new_date[:ok]
            subtotal = s.services.inject(0){|sum, se| sum + se.price}
            totals[:total] += subtotal
            payment_subscription = PaymentSuscription.new({:subscription_id => s.id, :subtotal => subtotal, :quantity => s.services.size, })
            payment_subscriptions.push(payment_subscription)
            s.update(:next_payment_date => new_date[:date])
          else
            raise new_date[:message]
          end
          p "Pago realizado correctamente para suscripcion #{s.id}"
        end
        payment = Payment.new({:client_id => subscription.first[:client_id], :date_payment => Time.now,  :total => totals[:total]})
        payment.payment_suscriptions = payment_subscriptions
        payment.save!
      else
        raise "Las suscripciones no se encuentran en el mismo periodo"
      end
    end

    payment
  end

  def validate_subscription_date(ids, date)
    date_payment = Time.parse(date.to_s).to_date
    subscription =  Subscription.where(:id => ids).map{|sub| Time.parse(sub.next_payment_date.to_s).to_date == date_payment}
    !subscription.include?(false)
  end

  def collection_services(services_ids)
    Service.where("id IN (?)", params[:subscription][:services])
  end
end