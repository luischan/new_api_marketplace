namespace :task_from_schedule do
  desc "TODOPURCHE"

  task :process_payment_subscription, [:message] => [:environment] do
    date = Time.now.strftime("%Y-%m-%d")
    begin
      include SubscriptionModule
      ActiveRecord::Base.transaction do

        suscriptions = Subscription.where("active = 1 AND DATE(next_payment_date) = DATE('#{date}')")
        unless suscriptions.empty?
          suscriptions_groups = suscriptions.group_by(&:client_id).collect {|key,value| value }
          suscriptions_groups.each do |subscription|
            ids = subscription.map(&:id)
            p "REALIZANDO PAGOS"
            p ids
            payment_subscription(ids)
          end
        end
      end
    rescue Exception => ex # optionally: `rescue Exception => ex`
      msj =  ex.message
      puts 'Pago no efectuado => ' + date.to_s
      puts msj
    end

  end
end
