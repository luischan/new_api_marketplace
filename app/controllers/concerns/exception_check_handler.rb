module ExceptionCheckHandler
    extend ActiveSupport::Concern
    included do
      rescue_from StandardError, with: :standard_error
      rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
      rescue_from ActiveRecord::RecordInvalid, with: :record_invalid
    end

    private

    def standard_error(_exception)
      json_response({ message: "An error occurred: #{_exception.message}" }, 500)
    end

    def record_not_found(_exception)
      json_response({ message: "Record not found" }, 404)
    end

    def record_invalid
      json_response({ message: "Record not process" }, 422)
    end
end