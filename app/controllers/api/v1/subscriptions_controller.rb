class Api::V1::SubscriptionsController < ApplicationController
  before_action :set_subscription, only: [:show, :update, :destroy]
  include SubscriptionModule
  # GET /subscriptions
  # GET /subscriptions.json
  def index
    @subscriptions = Subscription.all
  end

  # GET /subscriptions/1
  # GET /subscriptions/1.json
  def show
  end

  # POST /subscriptions
  # POST /subscriptions.json
  def create
    message = "Registro guardado correctamente"
    status = :created
    begin
      ActiveRecord::Base.transaction do
        generate_date =  calculate_next_date_payment(params[:subscription][:date_subscription], params[:subscription][:suscription_type])
        services = collection_services(params[:subscription][:services])
        if generate_date[:ok] && !services.empty?
          @subscription = Subscription.new(subscription_params)
          @subscription.next_payment_date = generate_date[:date]
          @subscription.services = collection_services(params[:subscription][:services])
          @subscription.save!
        else
          message = services.empty? ? "Servicios no encontrados" : generate_date[:message]
          status = :not_acceptable
        end
      end
      rescue StandardError => e
        status = :internal_server_error
        message = e.message
        @subscription = nil
      end
    json_response({:message=> message, item:@subscription }, status)
  end

  # PATCH/PUT /subscriptions/1
  # PATCH/PUT /subscriptions/1.json
  def update
    if @subscription.update(subscription_params)
      # render :show, status: :ok, location: @subscription
      render json: @subscription, status: :ok
    else
      render json: @subscription.errors, status: :unprocessable_entity
    end
  end

  # DELETE /subscriptions/1
  # DELETE /subscriptions/1.json
  def destroy
    @subscription.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.

    def set_subscription
      @subscription = Subscription.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def subscription_params
      params.require(:subscription).permit(
      :id,
          :date_subscription,
          :client_id,
          :suscription_type,
          :active,
          services_attributes:[
              :id
          ]
      )
    end
end
