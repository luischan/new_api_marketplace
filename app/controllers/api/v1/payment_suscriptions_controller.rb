class Api::V1::PaymentSuscriptionsController < ApplicationController
  before_action :set_payment_suscription, only: [:show, :update, :destroy]

  # GET /payment_suscriptions
  # GET /payment_suscriptions.json
  def index
    @payment_suscriptions = PaymentSuscription.all
  end

  # GET /payment_suscriptions/1
  # GET /payment_suscriptions/1.json
  def show
  end

  # POST /payment_suscriptions
  # POST /payment_suscriptions.json
  def create
    @payment_suscription = PaymentSuscription.new(payment_suscription_params)

    if @payment_suscription.save
      render :show, status: :created, location: @payment_suscription
    else
      render json: @payment_suscription.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /payment_suscriptions/1
  # PATCH/PUT /payment_suscriptions/1.json
  def update
    if @payment_suscription.update(payment_suscription_params)
      render :show, status: :ok, location: @payment_suscription
    else
      render json: @payment_suscription.errors, status: :unprocessable_entity
    end
  end

  # DELETE /payment_suscriptions/1
  # DELETE /payment_suscriptions/1.json
  def destroy
    @payment_suscription.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payment_suscription
      @payment_suscription = PaymentSuscription.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def payment_suscription_params
      params.fetch(:payment_suscription, {})
    end
end
