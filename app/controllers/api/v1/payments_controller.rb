class Api::V1::PaymentsController < ApplicationController
  before_action :set_payment, only: [:show, :update, :destroy]
  include PaymentModule
  include SubscriptionModule

  # GET /payments
  # GET /payments.json
  def index
    payments = get_payment(params)
    json_response(payments, :ok)
  end

  # GET /payments/1
  # GET /payments/1.json
  def show
  end

  # POST /payments
  # POST /payments.json
  def create
    message = "Registro guardado correctamente"
    status = :created
    payment = nil
    begin
      ActiveRecord::Base.transaction do
        suscription_ids = get_subscription(params).map(&:id)
        payment = payment_subscription(suscription_ids)
      end
    rescue StandardError => e
      status = :internal_server_error
      message = e.message
      payment = nil
    end
    json_response({data:payment, :message => message }, status)
  end

  # PATCH/PUT /payments/1
  # PATCH/PUT /payments/1.json
  def update
    if @payment.update(payment_params)
      render :show, status: :ok, location: @payment
    else
      render json: @payment.errors, status: :unprocessable_entity
    end
  end

  # DELETE /payments/1
  # DELETE /payments/1.json
  def destroy
    @payment.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payment
      @payment = Payment.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def payment_params
      params.fetch(:payment, {})
    end
end
