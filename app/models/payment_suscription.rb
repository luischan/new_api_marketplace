class PaymentSuscription < ApplicationRecord
  belongs_to :subscription
  belongs_to :payment
end
