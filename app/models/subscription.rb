class Subscription < ApplicationRecord

  # Se crea enum para describir el tipo de suscripcion.
  enum periodicity: {month: 1, year: 2}, _suffix: true

  has_and_belongs_to_many :services

  belongs_to :client

  has_many :payment_suscriptions

  has_many :payments, through: :payment_suscriptions

  accepts_nested_attributes_for :services

  validates_presence_of :date_subscription, :suscription_type, :client_id, :active
end
