class Payment < ApplicationRecord
  belongs_to :client
  has_many :payment_suscriptions
  has_many :subscriptions, through: :payment_suscriptions
end
