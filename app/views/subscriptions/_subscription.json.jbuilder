json.extract! subscription, :id, :created_at, :updated_at
json.url api_v1_subscription_url(subscription, format: :json)
