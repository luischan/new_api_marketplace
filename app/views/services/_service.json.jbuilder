json.extract! service, :id, :created_at, :updated_at
json.url api_v1_service_url(service, format: :json)
