json.extract! payment_suscription, :id, :created_at, :updated_at
json.url api_v1_payment_suscription_url(payment_suscription, format: :json)
