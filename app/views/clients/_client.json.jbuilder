json.extract! client, :id, :created_at, :updated_at
json.url api_v1_client_url(client, format: :json)
