# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version 2.6.3

* System dependencies

* Configuration
bundle install

* Database creation
mysql

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

bundle exec whenever

* Deployment instructions

API ROUTES

http://localhost:3000/api/v1/subscriptions

Example : {
        "subscription":{
        "date_subscription":"2020-09-11 19:26:23.581324",
        "client_id":"1",
        "suscription_type":"1",
        "active":"true",
        "services":["1","2"]
    }
}

http://localhost:3000//api/v1/payments

Example : {
"client_id":2,
"client_ids[]":1
"client_ids[]":2
"date_payment":2020-09-11 17:56:33.000000
"date_payment_last":2020-09-13 17:56:33.000000
"suscription_type":1
}

* ...
