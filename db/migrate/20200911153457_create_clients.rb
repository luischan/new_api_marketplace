class CreateClients < ActiveRecord::Migration[6.0]
  def change
    create_table :clients do |t|
      t.string :name
      t.string :lastname
      t.string :phone
      t.string :credit_card
      t.timestamps
    end
  end
end
