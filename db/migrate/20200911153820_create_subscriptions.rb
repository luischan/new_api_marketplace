class CreateSubscriptions < ActiveRecord::Migration[6.0]
  def change
    create_table :subscriptions do |t|
      t.datetime :date_subscription
      t.references :client, index:true, foreign_key:true
      t.integer :suscription_type
      t.boolean :active
      t.timestamps
    end
  end
end
