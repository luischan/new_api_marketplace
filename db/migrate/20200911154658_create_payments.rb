class CreatePayments < ActiveRecord::Migration[6.0]
  def change
    create_table :payments do |t|
      t.references :client, index:true, foreign_key:true
      t.datetime :date_payment
      t.float :total
      t.timestamps
    end
  end
end
