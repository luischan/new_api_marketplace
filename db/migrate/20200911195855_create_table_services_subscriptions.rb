class CreateTableServicesSubscriptions < ActiveRecord::Migration[6.0]
  def change
    create_table :services_subscriptions do |t|
      t.references :service, index:true, foreign_key:true
      t.references :subscription, index:true, foreign_key:true
    end
  end
end
