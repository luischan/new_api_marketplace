class CreatePaymentSuscriptions < ActiveRecord::Migration[6.0]
  def change
    create_table :payment_suscriptions do |t|
      t.references :payment, index:true, foreign_key:true
      t.references :subscription, index:true, foreign_key:true
      t.float :subtotal
      t.integer :quantity
      t.timestamps
    end
  end
end
