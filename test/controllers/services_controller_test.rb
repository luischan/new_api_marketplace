require 'test_helper'

class ServicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @service = services(:one)
  end

  test "should get index" do
    get api_v1_services_url, as: :json
    assert_response :success
  end

  test "should create service" do
    assert_difference('Service.count') do
      post api_v1_services_url, params: { service: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show service" do
    get api_v1_service_url(@service), as: :json
    assert_response :success
  end

  test "should update service" do
    patch api_v1_service_url(@service), params: { service: {  } }, as: :json
    assert_response 200
  end

  test "should destroy service" do
    assert_difference('Service.count', -1) do
      delete api_v1_service_url(@service), as: :json
    end

    assert_response 204
  end
end
