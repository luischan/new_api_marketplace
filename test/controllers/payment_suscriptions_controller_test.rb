require 'test_helper'

class PaymentSuscriptionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @payment_suscription = payment_suscriptions(:one)
  end

  test "should get index" do
    get api_v1_payment_suscriptions_url, as: :json
    assert_response :success
  end

  test "should create payment_suscription" do
    assert_difference('PaymentSuscription.count') do
      post api_v1_payment_suscriptions_url, params: { payment_suscription: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show payment_suscription" do
    get api_v1_payment_suscription_url(@payment_suscription), as: :json
    assert_response :success
  end

  test "should update payment_suscription" do
    patch api_v1_payment_suscription_url(@payment_suscription), params: { payment_suscription: {  } }, as: :json
    assert_response 200
  end

  test "should destroy payment_suscription" do
    assert_difference('PaymentSuscription.count', -1) do
      delete api_v1_payment_suscription_url(@payment_suscription), as: :json
    end

    assert_response 204
  end
end
