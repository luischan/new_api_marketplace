require 'test_helper'

class SubscriptionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @subscription = subscriptions(:one)
  end

  test "should get index" do
    get api_v1_subscriptions_url, as: :json
    assert_response :success
  end

  test "should create subscription" do
    client = Client.first
    assert_difference('Subscription.count') do
      post api_v1_subscriptions_url, params: { subscription: {
          "date_subscription":"2020-09-11 19:26:23.581324",
          "client_id":client.id,
          "suscription_type":"1",
          "active":"true",
          "services":["1","2"]
      } }, as: :json
    end

    assert_response 201
  end

  test "should show subscription" do
    get api_v1_subscription_url(@subscription), as: :json
    assert_response :success
  end

  test "should update subscription" do
    patch api_v1_subscription_url(@subscription), params: { subscription: {  } }, as: :json
    assert_response 200
  end

  test "should destroy subscription" do
    assert_difference('Subscription.count', -1) do
      delete api_v1_subscription_url(@subscription), as: :json
    end

    assert_response 204
  end
end
